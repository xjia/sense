#ifndef SENSE_H_
#define SENSE_H_

typedef nx_struct data_param_t {
	nx_uint16_t magic_num;
	nx_uint16_t node_id;
	nx_uint16_t seq_num;
	nx_uint16_t hop_num;
	nx_uint16_t temperature;
	nx_uint16_t humidity;
	nx_uint16_t checksum;
} data_param_t;

typedef nx_struct ctrl_param_t {
	nx_uint16_t magic_num;
	nx_uint16_t node_id;
	nx_uint16_t hop_num;
	nx_uint16_t seq_num;
	nx_uint16_t checksum;
} ctrl_param_t;

enum {
	DATA_MAGIC = 0xDEEE,
	CTRL_MAGIC = 0xCAFE,
	SAMPLE_PERIOD = 1500,
	DIJKSTRA_PERIOD = 5000,
	AM_BLINK_TO_RADIO = 6,
	AM_DATA_PARAM_T = 7
};

#endif
