#include <Timer.h>
#include "../Sense.h"

configuration CollectAppC {}

implementation {
	components MainC, LedsC;
	components CollectC as App;
	components new TimerMilliC() as SampleTimer;
	components ActiveMessageC;
	components new AMSenderC(AM_BLINK_TO_RADIO);
	components new AMReceiverC(AM_BLINK_TO_RADIO);
	components new SensirionSht11C() as SHT11C;
	
	App.Boot -> MainC.Boot;
	App.Leds -> LedsC;
	App.SampleTimer -> SampleTimer;
	App.Packet -> AMSenderC;
	App.AMPacket -> AMSenderC;
	App.AMSend -> AMSenderC;
	App.AMControl -> ActiveMessageC.SplitControl;
	App.Receive -> AMReceiverC;
	App.Temperature -> SHT11C.Temperature;
	App.Humidity -> SHT11C.Humidity;
}
