#include <Timer.h>
#include "../Sense.h"

module CollectC @safe() {
	uses interface Timer<TMilli> as SampleTimer;
	uses interface Leds;
	uses interface Boot;
	uses interface SplitControl as AMControl;
	uses interface Packet;
	uses interface AMPacket;
	uses interface AMSend;
	uses interface Receive;
	uses interface Read<uint16_t> as Temperature;
	uses interface Read<uint16_t> as Humidity;
}

implementation {
	message_t pkt;
	bool busy = FALSE;
	uint16_t temperature = 0;
	uint16_t humidity = 0;
	uint16_t sequence = 0;
	uint16_t hop_to_sink = -1;	// infinity
	uint16_t parent_node_id = 0;
	uint16_t hop_sequence = 0;
	
	uint16_t data_param_checksum(data_param_t* d) {
		uint16_t s = 0;
		s += d->magic_num;
		s += d->node_id;
		s += d->seq_num;
		s += d->hop_num;
		s += d->temperature;
		s += d->humidity;
		return ~s;
	}
	
	uint16_t ctrl_param_checksum(ctrl_param_t* c) {
		uint16_t s = 0;
		s += c->magic_num;
		s += c->node_id;
		s += c->hop_num;
		s += c->seq_num;
		return ~s;
	}
	
	event void Boot.booted() {
		call AMControl.start();
	}
	
	event void Temperature.readDone(error_t error, uint16_t data) {
		temperature = data;
	}
	
	event void Humidity.readDone(error_t error, uint16_t data) {
		humidity = data;
	}
	
	event void AMControl.startDone(error_t error) {
		if (error == SUCCESS) {
			call SampleTimer.startPeriodic(SAMPLE_PERIOD);
		} else {
			call AMControl.start();
		}
	}
	
	event void AMControl.stopDone(error_t error) {}
	
	event void SampleTimer.fired() {
		call Temperature.read();
		call Humidity.read();
		
		if (!busy) {
			data_param_t* dpkt = (data_param_t*) (call Packet.getPayload(&pkt, sizeof(data_param_t)));
			
			dpkt->magic_num = DATA_MAGIC;
			dpkt->node_id = TOS_NODE_ID;
			dpkt->seq_num = ++sequence;
			dpkt->hop_num = hop_to_sink;
			dpkt->temperature = temperature;
			dpkt->humidity = humidity;
			dpkt->checksum = data_param_checksum(dpkt);
			
			if (call AMSend.send(parent_node_id, &pkt, sizeof(data_param_t)) == SUCCESS) {
				busy = TRUE;
			}
		}
	}
	
	event void AMSend.sendDone(message_t* msg, error_t error) {
		if (&pkt == msg) {
			busy = FALSE;
		}
	}
	
	event message_t* Receive.receive(message_t* msg, void* payload, uint8_t len) {
		if (msg == NULL || payload == NULL) {
			return msg;
		} else if (len == sizeof(data_param_t)) {
			data_param_t* recv = (data_param_t*) payload;
			if (recv->magic_num == DATA_MAGIC && recv->checksum == data_param_checksum(recv)) {
				if (recv->hop_num > hop_to_sink && !busy) {
					data_param_t* dpkt = (data_param_t*) (call Packet.getPayload(&pkt, sizeof(data_param_t)));
					
					dpkt->magic_num = DATA_MAGIC;
					dpkt->node_id = recv->node_id;
					dpkt->seq_num = recv->seq_num;
					dpkt->hop_num = hop_to_sink;
					dpkt->temperature = recv->temperature;
					dpkt->humidity = recv->humidity;
					dpkt->checksum = data_param_checksum(dpkt);
					
					if (call AMSend.send(parent_node_id, &pkt, sizeof(data_param_t)) == SUCCESS) {
						busy = TRUE;
					}
				}
			}
		} else if (len == sizeof(ctrl_param_t)) {
			ctrl_param_t* recv = (ctrl_param_t*) payload;
			if (recv->magic_num == CTRL_MAGIC && recv->checksum == ctrl_param_checksum(recv)) {
				if (recv->hop_num + 1 < hop_to_sink || recv->seq_num != hop_sequence) {
					hop_to_sink = recv->hop_num + 1;
					hop_sequence = recv->seq_num;
					parent_node_id = recv->node_id;
					
					call Leds.set(parent_node_id);	// for debug
					
					if (!busy) {
						ctrl_param_t* cpkt = (ctrl_param_t*) (call Packet.getPayload(&pkt, sizeof(ctrl_param_t)));
						
						cpkt->magic_num = CTRL_MAGIC;
						cpkt->node_id = TOS_NODE_ID;
						cpkt->hop_num = hop_to_sink;
						cpkt->seq_num = hop_sequence;
						cpkt->checksum = ctrl_param_checksum(cpkt);
						
						if (call AMSend.send(AM_BROADCAST_ADDR, &pkt, sizeof(ctrl_param_t)) == SUCCESS) {
							busy = TRUE;
						}
					}
				}
			}
		}
		return msg;
	}
}