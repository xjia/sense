\documentclass[a4paper,10pt]{article}
\usepackage{amsmath, amsfonts, amsthm}

\usepackage[slantfont, boldfont, CJKtextspaces, CJKmathspaces]{xeCJK}
\usepackage{indentfirst}
\setmonofont{Nimbus Mono L}
\setCJKmainfont[BoldFont={Adobe Heiti Std}, ItalicFont={Adobe Fangsong Std}]{Adobe Song Std}
\setCJKsansfont{Adobe Heiti Std}
\setCJKmonofont{Adobe Kaiti Std}
\setlength{\parindent}{2em}
\linespread{1.2}
\renewcommand\labelitemi{\ensuremath{\bullet}}

\usepackage{graphicx}
\usepackage{tikz}
\usepackage{algpseudocode}

\usepackage{listings}
\lstset{language=C++}
\lstset{breaklines}
\lstset{extendedchars=false}

\usepackage{url}

\title{单汇点无线传感网络的实现}
\author{贾枭, 李诗剑, 王孟晖}

\begin{document}
\maketitle

\section{简介}
\label{sec:introduction}
本次实验中，我们使用一些无线传感节点，搭建了一个单汇点无线传感网络。
传感器节点是一个由电池供电的小型设备，它可以采集附近的温度和湿度信息，并且可以和其他节点进行无线通信。
我们设计了节点之间的路由算法和通信协议，实现了：
\begin{enumerate}
    \item 实时地显示所有传感器节点当前的温度和湿度信息；
    \item 自适应动态路由：无论何时调整节点的位置或者添加移除一些节点后，它们依然能够正常工作。
        并且路由算法能够保证每个节点选择的传输路径一定是最短的（跳数最少）；
    \item 错误检测：节点能够正确检测数据在传输过程中产生的错误；
    \item 图像输出：计算机通过采集到的数据，为每个节点绘制了温度、湿度变化曲线。
\end{enumerate}

此外，我们的所有协议都具有可扩展性，支持传输任意数据而不仅仅是温度信息。路由算法亦可应用到较多节点。

本文第~\ref{sec:overview}~节将从较高的抽象层面介绍整个系统，第~\ref{sec:routing}~节详细讨论路由协议和算法，第~\ref{sec:data}~节介绍数据传输协议，第~\ref{sec:result}~节将展示我们实验结果，最后第~\ref{sec:conclusion}~节总结我们的设计。

\section{系统概览}
\label{sec:overview}
在所有传感器节点中，有一个节点被称为\emph{Sink}。
它的主要责任有：
\begin{itemize}
    \item 采集附近的温度和湿度数据；
    \item 收集所有传感器节点发送过来的温度和湿度数据；
    \item 把收集到的数据通过USB接口发送给计算机。
\end{itemize}
除了Sink之外的所有其他节点，需要做的是：
\begin{itemize}
    \item 采集附近的温度和湿度数据；
    \item 把采集到的数据发送给Sink。
\end{itemize}

由于并非所有节点都能够直接和Sink通信，所以向Sink发送的数据包必须经由其他节点转发。
为了确定每个节点在发送数据时先发给谁，我们需要一个路由协议。

我们的路由协议采用了Dijkstra算法。
每个节点将会选择一个其他的节点，称为父节点，作为自己发送数据包的对象。
每个节点收到数据包后，也会把它转发到自己父节点。
经过几次转发后，数据包最终将到达Sink。

\begin{figure}
\label{fig:1}
\centering
\begin{tikzpicture}
    \tikzstyle{level 1}=[sibling distance=45pt]
    \tikzstyle{level 2}=[sibling distance=30pt]
    \tikzstyle{level 3}=[sibling distance=30pt]
    \tikzstyle{S}=[draw, minimum size=25pt, circle]
    \node[circle,draw] {Sink}
        child {node[S] {n1} child {node[S] {n5}}}
        child {node[S] {n2}}
        child {node[S] {n3}
            child {node[S] {n6}}
            child {node[S] {n7}}
        }
        child {node[S] {n4}};
\end{tikzpicture}
\caption{节点路由示意图}
\end{figure}

为了应对新增或者离开的节点，以及节点的位置变化，路由协议将会周期性地执行。

每个节点也会周期性地读取温度和湿度的测试值。
然后，它们把测试值发送给自己的父节点。
父节点将会转发数据包给父节点的父节点，最终数据包会发到Sink。
Sink得到的数据包都会给计算机。


\section{路由协议}
\label{sec:routing}
每隔一段时间，Sink会广播一个控制包。
控制包包含了节点号\texttt{node\_id}、距离Sink的跳数\texttt{hop\_num}、序列号\texttt{seq\_num}、以及校验和\texttt{checksum}。
Sink发出的控制包中，\texttt{hop\_num}等于$0$。
对于收到控制包的节点，它会查看包的序列号是否和当前保存的序列号相同。
如果不同，说明新一批的路由信息来了，需要更新自己的父节点，并广播转发这个控制包。
或者，如果控制包里跳数加一比当前的跳数更小，说明有一个更优选择，这时也需要更新并转发这个控制包。

\subsection{控制包定义}
\begin{lstlisting}
typedef nx_struct ctrl_param_t {
    nx_uint16_t magic_num;
    nx_uint16_t node_id;
    nx_uint16_t hop_num;
    nx_uint16_t seq_num;
    nx_uint16_t checksum;
} ctrl_param_t;
\end{lstlisting}
这里\texttt{magic\_num}是为了区分其他数据包。
\texttt{node\_id}表示发出这个包的节点的编号。
\texttt{hop\_num}表示这个节点距离Sink有几跳。
\texttt{seq\_num}是为了区分同一批控制包。
\texttt{checksum}把以上所有位加起来，用来检测传输过程中的比特错误。

\subsection{Sink节点路由算法}
Sink节点周期性地广播一个控制包。
包的\texttt{hop\_num}置为0。
每次广播的\texttt{seq\_num}递增。

\subsection{普通节点路由算法}
\begin{figure}[h!]
\caption{初始化}
\begin{algorithmic}
    \Procedure{Init}{}
        \State \texttt{hop\_num} = $\infty$
        \State \texttt{parent} = -1
        \State \texttt{hop\_seq} = -1
    \EndProcedure
\end{algorithmic}
\end{figure}

\begin{figure}[h!]
\caption{收到一个控制包时}
\begin{algorithmic}
    \Procedure{OnReceive}{\texttt{ctrl\_param\_t} \texttt{packet}}
        \If {\texttt{hop\_seq} $\neq$ \texttt{packet}.\texttt{seq\_num} or
            \texttt{packet}.\texttt{hop\_num} + 1<\texttt{hop\_num}}
            \State \texttt{hop\_seq} $\gets$ \texttt{packet}.\texttt{seq\_num}
            \State \texttt{hop\_num} $\gets$ \texttt{packet}.\texttt{hop\_num} + 1
            \State \texttt{parent} $\gets$ \texttt{packet}.\texttt{node\_id}
            \State BroadcastControlPacket(\texttt{my\_node\_id}, \texttt{hop\_num}, \texttt{hop\_seq})
        \EndIf
    \EndProcedure
\end{algorithmic}
\end{figure}

收到数据包时，只有产生更新或者新一批的数据包到来时，才会转发。
这样可以避免广播风暴的产生。

\section{数据传输协议}
当采集到温度和湿度的数据后，只需要向\texttt{parent}节点发送数据。
\texttt{parent}节点在收到数据之后，会负责把你的包通过其他人最终转发到Sink。

\subsection{数据包定义}
\label{sec:data}
\begin{lstlisting}
typedef nx_struct data_param_t {
    nx_uint16_t magic_num;
    nx_uint16_t node_id;
    nx_uint16_t seq_num;
    nx_uint16_t hop_num;
    nx_uint16_t temperature;
    nx_uint16_t humidity;
    nx_uint16_t checksum;
} data_param_t;
\end{lstlisting}
这里加入了\texttt{seq\_num}是为了避免数据包不按顺序到达，导致收到的温度、湿度信息并不是按照时间递增的顺序。


\section{实验结果}
\label{sec:result}
我们把上面的设计用nesC语言编程实现。
实验中，我们每10秒执行一遍路由协议，每0.6秒采集一次数据点。
我们用Java把采集到的温度和湿度信息绘制到了曲线上，如图~\ref{fig:screenshot}~所示。
\begin{figure}[hb]
    \label{fig:screenshot}
    \centering
    \includegraphics[scale=0.25]{screenshot.eps}
    \caption{程序运行截图}
\end{figure}


\section{总结}
\label{sec:conclusion}
本次实验中，我们为单汇点传感网络设计了路由协议和数据传输协议，使得所有传感器节点能够实时地向计算机返回温度和湿度信息。

路由协议具有较大的容错性，允许中途新增节点或者一些节点退出。
在节点位置发生变化的时候，也能够重新选择一条更短的路径进行数据传输。

数据传输和路由都进行了数据校验，保证数据包正确地被接收。
通过为数据包添加序列号，可以有效防止由于数据包不按顺序到达导致采集到的信息失真。

除了基本功能的实现，我们还将温度和湿度的信息绘制成曲线输出，从而可以更直观地看出各个节点处温度和湿度的变化。


\begin{thebibliography}{9}
\bibitem{tinyos}
    \emph{TinyOS Tutorials}.
    \url{http://docs.tinyos.net/tinywiki/index.php/TinyOS_Tutorials}
\end{thebibliography}

\end{document}
