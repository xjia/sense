#include <Timer.h>
#include "../Sense.h"

configuration SinkAppC {}

implementation {
	components MainC, LedsC;
	components SinkC as App;
	components new TimerMilliC() as SampleTimer;
	components new TimerMilliC() as DijkstraTimer;
	components ActiveMessageC;
	components new AMSenderC(AM_BLINK_TO_RADIO);
	components new AMReceiverC(AM_BLINK_TO_RADIO);
	components new SensirionSht11C() as SHT11C;
	components SerialActiveMessageC as SAM;
	
	App.Boot -> MainC.Boot;
	App.Leds -> LedsC;
	App.SampleTimer -> SampleTimer;
	App.DijkstraTimer -> DijkstraTimer;
	App.Packet -> AMSenderC;
	App.AMPacket -> AMSenderC;
	App.AMSend -> AMSenderC;
	App.AMControl -> ActiveMessageC.SplitControl;
	App.Receive -> AMReceiverC;
	App.Temperature -> SHT11C.Temperature;
	App.Humidity -> SHT11C.Humidity;
	App.SAMControl -> SAM;
	App.SAMSend -> SAM.AMSend[AM_DATA_PARAM_T];
	App.SAMPacket -> SAM;
}