public interface DataListener {
    public void notify(DataMessage msg);
}
