import java.awt.*;
import java.util.*;

import javax.swing.JFrame;

import org.jfree.chart.*;
import org.jfree.chart.axis.*;
import org.jfree.chart.plot.*;
import org.jfree.data.xy.*;

public class NodeFrame extends JFrame {
	
	private static final Dimension chartDimension = new Dimension(350, 180);
	
	private XYSeries tseries;	// for temperature
	private XYSeries hseries;	// for humidity
	
	public NodeFrame(String nodeId) {
		setTitle(nodeId);
		setLayout(new BorderLayout());
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		
		tseries = new XYSeries(nodeId, false, false);
		hseries = new XYSeries(nodeId, false, false);
		
		JFreeChart tchart = createChart(nodeId, new XYSeriesCollection(tseries));
		ChartPanel tchartPanel = new ChartPanel(tchart);
		tchartPanel.setPreferredSize(chartDimension);
		add(tchartPanel, BorderLayout.NORTH);
		
		JFreeChart hchart = createChart(nodeId, new XYSeriesCollection(hseries));
		ChartPanel hchartPanel = new ChartPanel(hchart);
		hchartPanel.setPreferredSize(chartDimension);
		add(hchartPanel, BorderLayout.SOUTH);
		
		pack();
		setLocationRelativeTo(null);
	}
	
	private static JFreeChart createChart(String title, XYDataset dataset) {
		JFreeChart chart = ChartFactory.createXYLineChart(
				title, "Time", "Value", dataset, PlotOrientation.VERTICAL, false, false, false);
		XYPlot plot = chart.getXYPlot();
		ValueAxis axis = plot.getDomainAxis();
		axis.setAutoRange(true);
		axis.setFixedAutoRange(100);
		axis = plot.getRangeAxis();
		axis.setAutoRange(true);
		return chart;
	}
	
	private LinkedList<Integer> temperatures = new LinkedList<Integer>();
	
	public synchronized void addTemperature(int t) {
		temperatures.add(t);
		tseries.add(temperatures.size(), t);
	}
	
	private LinkedList<Integer> humidities = new LinkedList<Integer>();
	
	public synchronized void addHumidity(int h) {
		humidities.add(h);
		hseries.add(humidities.size(), h);
	}
}