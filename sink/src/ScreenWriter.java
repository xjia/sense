public class ScreenWriter implements DataListener {
	
	private SinkUI ui;
	
	public ScreenWriter(SinkUI ui) {
		this.ui = ui;
	}

	public void notify(DataMessage msg) {
		System.out.println(String.format("node: %d\t seq: %d\t hop: %d\t temperature: %d\t humidity: %d",
				msg.get_node_id(), msg.get_seq_num(), msg.get_hop_num(), msg.get_temperature(), msg.get_humidity()));
		
		String nodeId = "Node " + msg.get_node_id();
		ui.ensureNodeId(nodeId);
		ui.get(nodeId).addTemperature(msg.get_temperature());
		ui.get(nodeId).addHumidity(msg.get_humidity());
	}
}
