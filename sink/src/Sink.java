import java.io.IOException;
import java.util.*;
import net.tinyos.message.*;
import net.tinyos.packet.*;
import net.tinyos.util.*;

public class Sink implements MessageListener {

	private MoteIF moteIF;
	private LinkedList<DataListener> listeners;

	public Sink(String source) {
		moteIF = new MoteIF(BuildSource.makePhoenix(source, PrintStreamMessenger.err));
		listeners = new LinkedList<DataListener>();
		moteIF.registerListener(new DataMessage(), this);
	}

	public void sendPackets() {}

	public void messageReceived(int to, Message message) {
		DataMessage msg = (DataMessage) message;
		notifyAll(msg);
	}

	public void addListener(DataListener l) {
		listeners.add(l);
	}

	private void notifyAll(DataMessage msg) {
		for(DataListener l : listeners) {
			l.notify(msg);
		}
	}
}
