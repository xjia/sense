public class SinkDemo {

    public static void main(String[] args) {
		if (args.length == 0) {
			System.err.println("Usage: java SinkDemo serial@/dev/ttyUSB0");
			return;
		}
		
    	Sink sink = new Sink(args[0]);
		sink.addListener(new ScreenWriter(new SinkUI()));
    }
}
