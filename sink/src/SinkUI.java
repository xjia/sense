import java.awt.*;
import java.awt.event.*;
import java.util.*;

import javax.swing.*;

public class SinkUI {
	
	public SinkUI() {
		initialize();
	}
	
	private JComboBox nodesCombo = new JComboBox();
	
	private HashSet<String> nodeIds = new HashSet<String>();
	
	public void ensureNodeId(String nodeId) {
		if (!nodeIds.contains(nodeId)) {
			nodeIds.add(nodeId);
			nodesCombo.addItem(nodeId);
		}
	}
	
	private void initialize() {
		JFrame toolkitFrame = new JFrame();
		toolkitFrame.setLayout(new FlowLayout());
		toolkitFrame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		
		nodesCombo.setEditable(false);
		toolkitFrame.add(nodesCombo);
		
		JButton nodesComboShow = new JButton("Show");
		nodesComboShow.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Object item = nodesCombo.getSelectedItem();
				if (item != null) {
					String nodeId = item.toString();
					if (nodeId != null && nodeId.trim().length() > 0) {
						get(nodeId).setVisible(true);
					}
				}
			}
		});
		toolkitFrame.add(nodesComboShow);
		
		toolkitFrame.pack();
		toolkitFrame.setVisible(true);
		toolkitFrame.setAlwaysOnTop(true);
		toolkitFrame.setSize(400, toolkitFrame.getHeight());
		toolkitFrame.setLocation(0, 0);
	}
	
	private Map<String, NodeFrame> nodeFrames = new HashMap<String, NodeFrame>();
	
	public synchronized NodeFrame get(String nodeId) {
		if (!nodeFrames.containsKey(nodeId)) {
			nodeFrames.put(nodeId, new NodeFrame(nodeId));
		}
		return nodeFrames.get(nodeId);
	}
}